﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class RayInteractable: MonoBehaviour
{
    public UnityEvent OnGazeEnter, OnGazeExit, OnActivate;

    public void FocusExit()
    {
        Debug.Log("exit");
        OnGazeExit.Invoke();
    }

    public void FocusEnter()
    {
        OnGazeEnter.Invoke();
    }

    public void Activate()
    {
        OnActivate.Invoke();
    }
}