﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRaycaster : MonoBehaviour
{
    [SerializeField]
    private float HitDistance;

    private RaycastHit currentHit, prevHit;
    private GameObject focusedObj, previousObj;


    public Action<GameObject> OnTargetChanged = (go) => { };


    private void Start()
    {
        OnTargetChanged += TargetChanged;
    }

    private void TargetChanged(GameObject obj)
    {
        if (previousObj!=focusedObj)
        {
            if (previousObj && previousObj.GetComponent<RayInteractable>()!=null)
            {
                previousObj.GetComponent<RayInteractable>().FocusExit();
            }          
        }

        previousObj = focusedObj;

        if (focusedObj && focusedObj.GetComponent<RayInteractable>() != null)
        {
            focusedObj.GetComponent<RayInteractable>().FocusEnter();
        }
    }

    void Update()
    {
        if (ShootRay())
        {
            if (prevHit.point != currentHit.point)
            {
                if (focusedObj!= currentHit.collider.gameObject)
                {
                    focusedObj = currentHit.collider.gameObject;
                    OnTargetChanged(focusedObj);         
                }
              
            }
            prevHit = currentHit;
        }
        else
        {
            if (focusedObj!=null)
            {
                focusedObj = null;
                OnTargetChanged(null);
            }
        }
    }

    private bool ShootRay()
    {
        Vector3 camAim = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        Ray ray = new Ray(camAim, Camera.main.transform.forward);

        Debug.DrawRay(ray.origin, ray.direction, Color.red);

        if (Physics.Raycast(ray, out currentHit, HitDistance))
        {
            return true;
        }
        return false;
    }
}
