﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionTooltip : MonoBehaviour
{
    [SerializeField]
    private GameObject View;

    [SerializeField]
    private SimpleRaycaster Raycaster;

    private RayInteractable currentInteractable;

    private void Start()
    {
        Raycaster.OnTargetChanged += TargetChanged;
        TargetChanged(null);
    }

    private void TargetChanged(GameObject obj)
    {
        if (obj==null || (obj!=null && obj.GetComponent<RayInteractable>() == null))
        {
            View.SetActive(false);
            currentInteractable = null;
        }
        else
        {
            View.SetActive(true);
            StartCoroutine(MoveTo(obj.transform.position));
            currentInteractable = obj.GetComponent<RayInteractable>();
        }

    }

    private IEnumerator MoveTo(Vector3 position)
    {
        Vector3 finalPos = position + ( Camera.main.transform.position- position).normalized*1f;
        Vector3 startPos = transform.position;
        float t = 0.5f;
        while (t>0)
        {
            transform.position = Vector3.Lerp(finalPos, startPos, t/0.5f);
            t -= Time.deltaTime;
            yield return null;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && currentInteractable)
        {
            currentInteractable.Activate();
        }
    }
}
